<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected  $table = 'states';

    protected $fillable = ['name','code'];

    protected $hidden = ['id', 'deleted_at', 'created_at', 'updated_at'];
}
