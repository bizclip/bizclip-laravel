<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventUser extends Model
{
    protected $table = 'event_users';

    protected $fillable = ['code', 'event_id', 'user_id', 'deleted_at', 'created_at', 'updated_at'];
}
