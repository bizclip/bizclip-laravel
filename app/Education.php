<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $table = 'educations';

    protected $fillable = ['name'];

    protected $hidden = ['deleted_at', 'created_at', 'updated_at', 'pivot'];
}
