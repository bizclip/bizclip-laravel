<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    protected $fillable = ['name','country_code','state_code'];

    protected $hidden = ['id', 'deleted_at', 'created_at', 'updated_at', 'state_code'];
}
