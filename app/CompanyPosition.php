<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPosition extends Model
{
    protected $table = 'company_positions';

    protected $fillable = ['name','description','level'];
}
