<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interest extends Model
{
    use SoftDeletes;

    protected $table = 'interests';

    protected $fillable = ['name', 'description'];

    protected $hidden = ['deleted_at', 'created_at', 'updated_at', 'pivot'];
}
