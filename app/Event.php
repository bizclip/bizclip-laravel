<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected  $table = 'events';

    protected $fillable = [
        'owner_id',
        'name',
        'description',
        'start',
        'end',
        'address',
        'city_id',
        'state_id',
        'country_id'
    ];

    protected $hidden = ['deleted_at', 'created_at', 'updated_at', 'owner_id', 'city_id', 'state_id', 'country_id', 'pivot'];

    public function owners()
    {
        return $this->belongsTo('App\User', 'owner_id');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }

    public function state()
    {
        return $this->belongsTo('App\State', 'state_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }
}
