<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     *
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('oib', function($attribute, $value, $parameters, $validator) {
            if (strlen($value) == 11) {
                $val = 10;

                for ($i = 0; $i < 10; ++$i) {
                    $val += $value[$i];
                    $val %= 10;

                    if ($val == 0) {
                        $val = 10;
                    }

                    $val *= 2;
                    $val %= 11;
                }

                $control = 11 - $val;

                if ($control == 10) {
                    $control = 0;
                }

                return $control == $value[10];
            }

            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
