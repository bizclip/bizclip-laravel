<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected  $table = 'countries';

    protected $fillable = ['name','code'];

    protected $hidden = ['id', 'deleted_at', 'created_at', 'updated_at'];
}
