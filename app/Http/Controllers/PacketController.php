<?php

namespace App\Http\Controllers;

use App\Packet;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class PacketController extends Controller
{
    /**
     * @GET("/api/purchase", as="api.packet.index")
     *
     * Display available packets
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ret['response'] = Packet::all();

        return Response::json($ret, HTTP_OK);
    }
}
