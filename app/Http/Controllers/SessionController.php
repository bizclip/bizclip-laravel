<?php

namespace App\Http\Controllers;

use Collective\Annotations\Routing\Annotations\Annotations\Middleware;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Response;
use Input;
use Tymon\JWTAuth\Facades\JWTAuth;

class SessionController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.refresh', ['only' => ['refreshToken']]);
        $this->middleware('credentials.validation', ['only' => ['postLogin']]);
    }

    /**
     * @POST("/api/login", as="api.login")
     */
    public function postLogin(Request $request)
    {
        $data = $request->only('email', 'password');

        if ($token = JWTAuth::attempt($data)) {
            $ret['response']['token'] = $token;
            $ret['response']['user'] = Auth::user();

            return Response::json($ret, HTTP_OK);
        }

        $ret['response']['message'] = "Wrong username or password.";

        return Response::json($ret, HTTP_BAD_REQUEST);
    }

    /**
     * @GET("/api/token", as="api.token")
     */
    public function refreshToken()
    {
        // token refresh
    }
}
