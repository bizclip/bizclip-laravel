<?php

namespace App\Http\Controllers;

use App\Event;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Exception;
use App\EventUser;

/**
 * @Controller(prefix="api")
 */
class UserEventController extends Controller
{
    public function __construct()
    {
        $this->middleware('authorize.request');
        $this->middleware('token.auth');
        $this->middleware('jwt.refresh');
    }

    /**
     * @PUT("/users/{userId}/events/{eventId}", as="api.users.events.update")
     *
     * When user wants to join an event
     *
     * @param Request $request
     * @param $userId
     * @param $eventId
     * @return Response
     */
    public function update(Request $request, $userId, $eventId)
    {
        $data = $request->only(['code']);

        $rules = [
            'code' => 'required|exists:event_users,code,is_valid,1'
        ];

        $validator = Validator::make($data, $rules);

        if (!$validator->passes()) {
            $ret['response'] = $validator->errors();

            return response($ret, HTTP_BAD_REQUEST);
        }

        $eventUser = EventUser::where('code', $data['code'])
                ->where('event_id', $eventId)
                ->where('is_valid', 1)
                ->update(['is_valid' => 0, 'user_id' => $userId]);

        if ($eventUser) {
            $ret['response']['message'] = "Joined event!";

            return Response::json($ret, HTTP_OK);
        }

        $ret['response']['message'] = "Couldn't join event.";

        return Response::json($ret, HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @GET("/users/{userId}/events/hosted", as="api.user.events.hosted")
     *
     * @param $userId
     * @return mixed
     */
    public function getHostedEvents($userId)
    {
        $ret['response'] = Event::with('owners')->where('owner_id', $userId)->get();

        return Response::json($ret, HTTP_OK);
    }

    /**
     * @GET("/users/{userId}/events/joined", as="api.user.events.joined")
     *
     * @param $userId
     * @return mixed
     */
    public function getJoinedEvents($userId)
    {
        $ret['response'] = User::with('events')->find($userId);

        return Response::json($ret, HTTP_OK);
    }
}
