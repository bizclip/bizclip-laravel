<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('authorize.request', ['only' => ['store', 'update']]);
        $this->middleware('token.auth', ['except' => ['getAllEvents', 'getUpcomingEvents']]);
        $this->middleware('jwt.refresh', ['except' => ['getAllEvents', 'getUpcomingEvents']]);
    }

    /**
     * @GET("/api/event/all", as="api.event.all")
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllEvents()
    {
        $ret['response'] = Event::with('country')->with('state')->with('city')->get();

        return Response::json($ret, HTTP_OK);
    }

    /**
     * @GET("/api/event/upcoming", as="api.event.upcoming")
     *
     * @return \Illuminate\Http\Response
     */
    public function getUpcomingEvents()
    {
        $ret['response'] = Event::with('country')->with('state')->with('city')->where('end', '>', DB::RAW('NOW()'))->get();

        return Response::json($ret, HTTP_OK);
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     *
     * Should NEVER be visible to public, this method will be
     * triggered after successful payment
     *
     * @param $data
     * @return Response
     *
     */
    private function store($data)
    {
        $data['owner_id'] = JWTAuth::parseToken()->toUser()->id;

        $validator = $this->eventValidator($data);

        if (!$validator->passes()) {
            $ret['response'] = $validator->errors();

            return response($ret, HTTP_BAD_REQUEST);
        }

        if (!is_null(Event::create($data))) {
            $ret['response']['message'] = "Event created!";

            return response($ret, HTTP_OK);
        }

        $ret['response']['message'] = "Failed to create new event.";

        return response($ret, HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @GET("/api/event/{id}", as="api.event.show")
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ret['response'] = Event::with('country')->with('state')->with('city')->where('id', $id)->get();

        return Response::json($ret, HTTP_OK);
    }

    /**
     * @PUT("/api/event/{id}", as="api.event.update")
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only(['name', 'description', 'start', 'end', 'address', 'city_id', 'state_id', 'country_id']);

        $data['owner_id'] = JWTAuth::parseToken()->toUser()->id;

        $validator = $this->eventValidator($data);

        if (!$validator->passes()) {
            $ret['response'] = $validator->errors();

            return response($ret, HTTP_BAD_REQUEST);
        }

        if (Event::where('id', $id)->where('end', '>', DB::raw('NOW()'))->update($data)) {
            $ret['response']['message'] = "Event updated!";

            return response($ret, HTTP_OK);
        }

        $ret['response']['message'] = "Failed to update event info.";

        return response($ret, HTTP_INTERNAL_SERVER_ERROR);
    }

    private function eventValidator($data)
    {
        $rules = [
            'owner_id' => 'required|exists:users,id',
            'name' => 'required|min:1|max:100',
            'description' => 'required|min:100|max:255',
            'start' => 'required|date|date_format:Y-m-d H:i:s|after:tomorrow',
            'end' => 'required|date|date_format:Y-m-d H:i:s|after:start',
            'address' => 'required|min:5|max:100',
            'city_id' => 'required|exists:cities,id',
            'state_id' => 'exists:states,id',
            'country_id' => 'required|exists:countries,id'
        ];

        return Validator::make($data, $rules);
    }
}
