<?php

namespace App\Http\Controllers;

use App\Packet;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

/**
 * @Controller(prefix="api")
 */
class UserTransactionController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;

        $this->middleware('authorize.request');
        $this->middleware('token.auth');
        $this->middleware('jwt.refresh');
    }

    /**
     * @GET("/users/{userId}/transactions", as="api.user.transactions.index")
     *
     * Purchase history by user
     *
     * @param $userId
     * @return \Illuminate\Http\Response
     */
    public function index($userId)
    {

    }

    /**
     * @POST("/users/{userId}/transactions")
     *
     * @param $userId
     * @param $packetId
     */
    public function store($userId)
    {
//        if (User::find($userId)->has_purchase) {
//            return $this->user->storeTransaction($userId, $packetId);
//        }
    }

    /**
     * @GET("/users/{userId}/transactions/{transactionId}", as="api.user.transactions.show")
     *
     * Purchase details by user
     *
     * @param $userId
     * @param $transactionId
     * @return \Illuminate\Http\Response
     */
    public function show($userId, $transactionId)
    {
        //
    }
}
