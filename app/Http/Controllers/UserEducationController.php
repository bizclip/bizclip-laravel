<?php

namespace App\Http\Controllers;

use App\UserEducation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Response;

class UserEducationController extends Controller
{
    public function __construct()
    {
        $this->middleware('authorize.request');
        $this->middleware('token.auth');
        $this->middleware('jwt.refresh');
    }

    /**
     * @POST("/api/user/{user_id}/education/{education_id}", as="api.user.education.store")
     *
     * @param $userId
     * @param $educationId
     * @return Response
     */
    public function store($userId, $educationId)
    {
        $data = [
            $userId,
            $educationId,
            Carbon::now()->toDateTimeString()
        ];

        $isInserted = DB::insert(
            'INSERT INTO user_educations (`user_id`, `education_id`, `created_at`) VALUES (?, ?, ?)
             ON DUPLICATE KEY UPDATE `updated_at` = "' . Carbon::now()->toDateTimeString() .'", `deleted_at` = ' . DB::raw('NULL'), $data
        );

        if ($isInserted) {
            $ret['response']['message'] = "Created.";

            return Response::json($ret, HTTP_OK);
        }

        $ret['response']['message'] = "Internal server error.";

        return Response::json($ret, HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @GET("/api/user/{user_id}/education", as="api.user.education.show")
     *
     * Display the specified resource.
     *
     * @param  int  $userId
     * @return \Illuminate\Http\Response
     */
    public function show($userId)
    {
        $ret['response'] = UserEducation::with('education')->where('user_id', $userId)->whereNull('deleted_at')->get();

        return Response::json($ret, HTTP_OK);
    }

    /**
     * @DELETE("/api/user/{user_id}/education/{education_id}", as="api.user.education.destroy")
     *
     * Remove the specified resource from storage.
     *
     * @param  int  $userId
     * @param  int  $educationId
     * @return \Illuminate\Http\Response
     */
    public function destroy($userId, $educationId)
    {
        if (UserEducation::where(['user_id' => $userId, 'education_id' => $educationId])->delete()) {
            $ret['response']['message'] = "Deleted.";
            return Response::json($ret, HTTP_OK);
        }

        $ret['response']['message'] = "Bad request.";

        return Response::json($ret, HTTP_BAD_REQUEST);
    }
}
