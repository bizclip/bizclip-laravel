<?php

namespace App\Http\Controllers;

use App\UserInterest;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Carbon\Carbon;
use DB;

class UserInterestController extends Controller
{
    public function __construct()
    {
        $this->middleware('authorize.request');
        $this->middleware('token.auth');
        $this->middleware('jwt.refresh');
    }

    /**
     * @POST("/api/user/{user_id}/interest/{interest_id}", as="api.user_interest.store")
     *
     * @param $userId
     * @param $interestId
     * @return Response
     */
    public function store($userId, $interestId)
    {
        $data = [
            $userId,
            $interestId,
            Carbon::now()->toDateTimeString()
        ];

        $isInserted = DB::insert(
            'INSERT INTO user_interests (`user_id`, `interest_id`, `created_at`) VALUES (?, ?, ?)
             ON DUPLICATE KEY UPDATE `updated_at` = "' . Carbon::now()->toDateTimeString() .'", `deleted_at` = ' . DB::raw('NULL'), $data
        );

        if ($isInserted) {
            $ret['response']['message'] = "Created.";

            return Response::json($ret, HTTP_CREATED);
        }

        $ret['response']['message'] = "Internal server error.";

        return Response::json($ret, HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @GET("/api/user_interest/{id}", as="api.user_interest.show")
     *
     * Display the specified resource.
     *
     * @param  int  $userId
     * @return \Illuminate\Http\Response
     */
    public function show($userId)
    {
        $ret['response'] = UserInterest::with('interest')->where('user_id', $userId)->whereNull('deleted_at')->get();

        return Response::json($ret, HTTP_OK);
    }

    /**
     * @DELETE("/api/user/{user_id}/interest/{interest_id}", as="api.user_interests.destroy")
     *
     * Remove the specified resource from storage.
     *
     * @param  int  $userId
     * @param  int  $interestId
     * @return \Illuminate\Http\Response
     */
    public function destroy($userId, $interestId)
    {
        if (UserInterest::where(['user_id' => $userId, 'interest_id' => $interestId])->delete()) {
            $ret['response']['message'] = "Deleted.";
            return Response::json($ret, HTTP_OK);
        }

        // thrown when already deleted / doesn't exist
        $ret['response']['message'] = "Bad request.";

        return Response::json($ret, HTTP_BAD_REQUEST);
    }
}
