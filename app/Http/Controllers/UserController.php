<?php

namespace App\Http\Controllers;

use App\User;
use App\UserEducation;
use App\UserInterest;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('authorize.request');
        $this->middleware('token.auth', ['except' => ['store']]);
        $this->middleware('jwt.refresh', ['except' => ['store']]);
    }

    /**
     * @POST("/api/register", as="api.register")
     *
     * Register new user
     *
     * @param Request $request
     * @return
     */
    public function store(Request $request)
    {
        $data = $request->only('first_name', 'last_name', 'email', 'password', 'password_confirmation');
        $validator = $this->registrationValidator($data);

        if ($validator->passes()) {
            $data['password'] = bcrypt($data['password']);
            $data['user_type_id'] = 2;

            User::create($data);

            $ret['response']['message'] = "Registration successful!";

            return Response::json($ret, HTTP_OK);
        }

        $ret['response'] = $validator->messages();

        return Response::json($ret, HTTP_BAD_REQUEST);
    }

    private function registrationValidator($data)
    {
        $rules = [
            'first_name' => 'required|min:2|max:20',
            'last_name' => 'required|max:30',
            'email' => 'required|email|max:254|unique:users',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6'
        ];

        return Validator::make($data, $rules);
    }

    /**
     * @GET("/api/user/{id}", as="api.user.show")
     *
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ret['response']['user'] = User::with('interests')->with('educations')->find($id);

        return Response::json($ret, HTTP_OK);
    }

    /**
     * @PUT ("/api/user/{user_id}", as="api.user.update")
     *
     * Avatar update should be extra; CV update should be extra
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $userId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $userId)
    {
        $data = $request->only([
            'company_id', 'position_id', 'first_name', 'last_name', 'birthday',
            'phone', 'mobile', 'years_of_experience', 'OIB', 'interests', 'educations'
        ]);

        $validator = $this->updateValidator($data);

        if (!$validator->passes()) {
            $ret['response'] = $validator->errors();

            return response($ret, HTTP_BAD_REQUEST);
        }

        if (isset($data['interests'])) {
            $this->updateInterests($data['interests'], $userId);
            unset($data['interests']);
        }

        if (isset($data['educations'])) {
            $this->updateEducations($data['educations'], $userId);
            unset($data['educations']);
        }

        $user = User::find($userId);

        foreach ($data as $key => $value) {
            if (!is_null($value)) {
                $user->{$key} = $value;
            }
        }

        $user->save();

        $ret['response']['message'] = "Updated.";

        return Response::json($ret, HTTP_OK);
    }

    private function updateInterests($interests, $userId)
    {
        $userInterestController = new UserInterestController();
        $userInterests = UserInterest::where('user_id', $userId)->get();

        // can be improved with binary search
        foreach ($userInterests as $userInterest) {
            $currInterestId = $userInterest['interest_id'];
            $found = false;

            foreach ($interests as $newUserInterest) {
                if ($currInterestId == $newUserInterest['interest_id']) {
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $userInterestController->destroy($userId, $currInterestId);
            }
        }

        foreach ($interests as $userInterest) {
            if ($userInterestController->store($userId, $userInterest['interest_id'])->status() == HTTP_INTERNAL_SERVER_ERROR) {
                $ret['response']['message'] = "Internal server error.";
                return Response::json($ret, HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }

    private function updateEducations($educations, $userId)
    {
        $userEducationController = new UserEducationController();
        $userEducations = UserEducation::where('user_id', $userId)->get();

        // can be improved with binary search
        foreach ($userEducations as $userEducation) {
            $currEducationId = $userEducation['education_id'];
            $found = false;

            foreach ($educations as $newUserEducation) {
                if ($currEducationId == $newUserEducation['education_id']) {
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $userEducationController->destroy($userId, $currEducationId);
            }
        }

        foreach ($educations as $userEducation) {
            if ($userEducationController->store($userId, $userEducation['education_id'])->status() == HTTP_INTERNAL_SERVER_ERROR) {
                $ret['response']['message'] = "Internal server error.";
                return Response::json($ret, HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }

    private function updateValidator($data)
    {
        $rules = [
            'company_id' => 'exists:companies,id',
            'position_id' => 'exists:company_positions,id',
            'first_name' => 'min:2|max:20',
            'last_name' => 'max:30',
            'birthday' => 'date_format:Y-m-d H:i:s',
            'phone' => 'min:6|max:15',
            'mobile' => 'min:6|max:15',
            'years_of_experience' => 'integer',
            'OIB' => 'oib',
            'interests' => 'array',
            'educations' => 'array'
        ];

        return Validator::make($data, $rules);
    }

    /**
     * @DELETE("/api/user/{user_id}", as="api.user.destroy")
     *
     * Remove the specified resource from storage.
     *
     * @param  int  $userId
     * @return \Illuminate\Http\Response
     */
    public function destroy($userId)
    {
        if (User::find($userId)->delete()) {
            $ret['response']['message'] = "Deleted.";
            return Response::json($ret, HTTP_OK);
        }

        $ret['response']['message'] = "Bad request.";

        return Response::json($ret, HTTP_BAD_REQUEST);
    }
}
