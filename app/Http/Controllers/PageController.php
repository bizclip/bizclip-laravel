<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('csrf.token', ['only' => ['getRegister', 'getLogin']]);
        $this->middleware('token.auth', ['except' => ['getRegister', 'getLogin']]);
        $this->middleware('jwt.refresh', ['except' => ['getRegister', 'getLogin']]);
    }

    /**
     * @GET("/login", as="page.login")
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin()
    {
        return view('pages.login');
    }

    /**
     * @GET("/register", as="page.register");
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRegister()
    {
        return view('pages.register');
    }
}
