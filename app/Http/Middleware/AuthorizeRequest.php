<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Response;

class AuthorizeRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (isset($request->userId)) {
                if (JWTAuth::parseToken()->toUser()->id !== $request->userId) {
                    $ret['response']['message'] = 'Unauthorized.';

                    return Response::json($ret, HTTP_UNAUTHORIZED);
                }
            }
        } catch (JWTException $e) {
            $ret['response']['message'] = $e->getMessage();

            return Response::json($ret, $e->getStatusCode());
        }

        return $next($request);
    }
}
