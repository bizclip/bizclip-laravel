<?php

namespace App\Http\Middleware;

use Closure;
use Response;
use Validator;

class CredentialsValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = $request->only('email', 'password');

        $validator = Validator::make($data, [
            'email' => 'required',
            'password' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            $ret['response']['message'] = $validator->errors();

            return Response::json($ret, HTTP_BAD_REQUEST);
        }

        return $next($request);
    }
}
