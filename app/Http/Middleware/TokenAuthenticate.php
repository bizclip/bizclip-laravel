<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Response;

class TokenAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (!JWTAuth::parseToken()->authenticate()) {
                $ret['response']['message'] = 'User not found.';

                return Response::json($ret, HTTP_UNAUTHORIZED);
            }
        } catch (JWTException $e) {
            $ret['response']['message'] = $e->getMessage();

            return Response::json($ret, $e->getStatusCode());
        }

        return $next($request);
    }
}
