<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserEducation extends Model
{
    use SoftDeletes;

    protected $table = 'user_educations';

    protected $primaryKey = ['user_id', 'education_id'];

    protected $fillable = ['education_id','user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function education()
    {
        return $this->belongsTo('App\Education');
    }
}
