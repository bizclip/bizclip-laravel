<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserInterest extends Model
{
    use SoftDeletes;

    protected $table = 'user_interests';

    protected $primaryKey = ['user_id', 'interest_id'];

    protected $fillable = ['user_id', 'interest_id'];

    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];
}
