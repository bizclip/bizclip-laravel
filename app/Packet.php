<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packet extends Model
{
    protected $table = 'packets';

    protected $fillable = ['price','description','name'];
}
