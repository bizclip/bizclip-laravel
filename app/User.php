<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;

    protected $table = 'users';

    protected $fillable = [
        'company_id',
        'position_id',
        'user_type_id',
        'first_name',
        'last_name',
        'email',
        'password',
        'birthday',
        'avatar',
        'phone',
        'mobile',
        'years_of_experience',
        'cv',
        'OIB'
    ];

    protected $hidden = ['password', 'remember_token', 'deleted_at', 'created_at', 'updated_at'];

    public function interests()
    {
        return $this->belongsToMany('App\Interest', 'user_interests');
    }

    public function educations()
    {
        return $this->belongsToMany('App\Education', 'user_educations');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event', 'event_users', 'user_id', 'event_id');
    }
}
