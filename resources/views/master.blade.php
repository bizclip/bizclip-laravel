<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BizClip - @yield('title')</title>
        <!-- CSS -->
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('css/main.css') }}" /> <!-- Main CSS -->
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('css/stuff.css') }}" /> <!-- Main CSS -->
        <!-- End of CSS -->

        <!-- FONTS -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <!-- End of FONTS -->

        <!-- META -->
        <meta name="viewport" content="width=device-width,initial-scale=1.0" />
        <!-- End of META -->

</head>
<body>
    <div class="container">
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="{{URL::asset('js/token.js')}}"></script>
    <script src="{{URL::asset('js/notify.min.js')}}"></script>
    @yield('footer')
</body>
</html>