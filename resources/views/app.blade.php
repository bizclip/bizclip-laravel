<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link type="text/css" rel="stylesheet" href="css/main.css" /> <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="css/header.css" /> <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="css/stuff.css" /> <!-- Main CSS -->
    <title>
        @yield('title')
    </title>
    <!-- FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- End of FONTS -->
</head>
<body>

    @yield('content')


@yield('footer')
</body>
</html>