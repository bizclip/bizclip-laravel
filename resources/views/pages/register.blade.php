@extends('master')

@section('title', 'Register')

@section('content')
<div class="header">
    <div class="nav-bar round8">
        <div class="w20 logo">
            <img src="img/main/logo.jpg" />
        </div>
        <div class="w80 links">
            <ul>
                <li class="round5"><a href="/">HOME</a></li>
                <li class="round5"><a href="/conferences">CONFERENCES</a></li>
                <li class="round5"><a href="/profile">PROFILE</a></li>
                <li class="round5"><a href="/about">ABOUT US</a></li>
            </ul>
        </div>
    </div>

    <div class="mid-text">

    </div>
</div>
<div class="register">
    <h3>Hello and welcome to Bizclip. You can register here, just fill in this few boxes below and click on button register.!</h3>
    <form action="" id="forma"><table>
            <tr>
                <td> First name:</td>
                <td><input type="text" class="round5" name="fname" placeholder="First Name"/></td>
            </tr>
            <tr>
                <td>Last name: </td>
                <td><input type="text" class="round5" name="lname" placeholder="Last Name"/></td>
            </tr>
            <tr>
                <td>  Email: </td>
                <td><input type="email" class="round5" name="email" placeholder="Email"/></td>
            </tr>
            <tr>
                <td>Password: </td>
                <td><input type="password" class="round5" name="pass" placeholder="Password"/></td>
            </tr>
            <tr>
                <td>Re-password:</td>
                <td><input type="password" class="round5" name="repass" placeholder="Repeat password"/></td>
            </tr>
            <tr>
                <td colspan="2"> <input type="checkbox" name="terms"> I agree with terms of usage!</td>
            </tr>
            <tr></form>
                <td colspan="2" style="text-align:center;"><button  type="button" onClick="register();">REGISTER</button></td>
            </tr>
</div>
@stop

@section('footer')
    <script src="js/register.js"></script>
@stop