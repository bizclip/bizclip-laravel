@extends('master')

@section('title', 'Login')

@section('content')
        <div class="topImg border-box">
            <div class="nav-bar round8 allShadow">
                <div class="logo border-box">
                    <img src="img/main/logo.jpg" />
                </div>
                <div class="links">
                    <ul>
                        <li class="transitionAll07s"><a href="/" class="transitionAll07s">HOME</a></li>
                        <li class="transitionAll07s"><a href="/conferences" class="transitionAll07s">CONFERENCES</a></li>
                        <li class="transitionAll07s"><a href="/profile" class="transitionAll07s">PROFILE</a></li>
                        <li class="transitionAll07s"><a href="/about" class="transitionAll07s">ABOUT US</a></li>
                    </ul>
                </div>
            </div>

        </div><!-- End of topImg -->
        <div class="steps">
            <div class="mess round5 allShadow">
                <h2>You can login by filling these fields, if you don't have an account you can register <a href="/register/">here</a>.</h2>
                <form action="" method="POST">
                    <table>
                        <tr><td class="left">Email:</td><td class="right"><input type="text" name="email" class="round5 border-box" /></td></tr>
                        <tr><td class="left">Password:</td><td class="right"><input type="password" name="pass" class="round5 border-box" /></td></tr>
                        <tr><td colspan="2"><button onClick="login();" type="button" class="round5 transitionAll07s">Login</button></td></tr>
                    </table>
                </form>

            </div>

        </div><!-- End of steps -->
        <div class="newsletter">
            Subscribe to our newsletter for news about new conferences! <input type="email" class="round5 border-box" placeholder="Email"> <button class="round5 transitionAll07s">Subscribe!</button>
        </div>



@endsection

@section('footer')
        <script src="{{ URL::asset('js/login.js')  }}"></script>
@endsection