function register(){



    var fname = $("input[name=fname]").val();
    var lname = $("input[name=lname]").val();
    var email = $("input[name=email]").val();
    var pass = $("input[name=pass]").val();
    var repass = $("input[name=repass]").val();


    var credentials = { "first_name":fname, "last_name":lname, "email":email, "password":pass, "password_confirmation":repass};
    $.ajax({
        url: ROOT_URL+'/api/register',
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        dataType: "json",
        data: JSON.stringify(credentials),
        success: function(data) {
           $.notify("You have been successfully registered, I'll transfer you to login page in a few seconds ;)", {
                clickToHide: true,
                autoHide: true,
                position: 'top center',
                className: 'success'
            });
            transfer();
        },
        error: function(data, status, error) {
            $.notify("There is some problem with your registration try again! ;)", {
                clickToHide: true,
                autoHide: true,
                position: 'top center',
                className: 'error'
            });
        }
    });
}
function transfer(token){
    setTimeout(function(){
        window.location.replace('/login');
    }, 3000)
}