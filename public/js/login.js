function login(){
    var username = $("input[name=email]").val();
    var password = $("input[name=pass]").val();

    var credentials = { "email":username, "password":password};
    $.ajax({
        url: ROOT_URL+'/api/login',
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        dataType: "json",
        data: JSON.stringify(credentials),
        success: function(data) {
            $.notify("You have been logged in! I'll transfer you in a few seconds ;)", {
                clickToHide: true,
                autoHide: true,
                position: 'top center',
                className: 'success'
            });
            console.log(data.response.token);
            sessionStorage.token = data.response.token;
            transfer(data.response.token);
        },
        error: function(data, status, error) {
            $.notify("Something went wrong, try again!", {
                clickToHide: true,
                autoHide: true,
                position: 'top center',
                className: 'error',
            });
            sessionStorage.token = "";
        }
    });
}
function transfer(token){
    setTimeout(function(){
        window.location.replace('/dashboard?token='+token);
    }, 3000)
}