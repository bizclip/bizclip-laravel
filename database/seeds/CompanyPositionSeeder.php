<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CompanyPositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $positions = fopen(dirname(__FILE__) . "/data/company_positions", "r");
        $data = [];
        $level = 0;

        while (($row = fgets($positions)) !== false) {
            $val = explode(";", $row);

            $data[] = [
                'name' => trim($val[0]),
                'description' => trim($val[1]),
                'level' => ++$level,
                'created_at' => $faker->dateTimeThisDecade($max = 'now'),
                'updated_at' => $faker->dateTimeThisDecade($max = 'now')
            ];
        }

        fclose($positions);

        DB::table('company_positions')->insert($data);
    }
}
