<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $data = [];

        for ($i = 0; $i < 500; ++$i) {
            $cityId = $faker->numberBetween(1, 2559529);

            $cityRow = DB::table('cities')->where('id', $cityId)->first();
            $stateId = DB::table('states')->where('code', $cityRow->state_code)->first();
            $countryId = DB::table('countries')->where('code', $cityRow->country_code)->first();

            $data[] = [
                'owner_id' => $faker->numberBetween(1, 10000),
                'city_id' => $cityId,
                'state_id' => empty($stateId) ? null : $stateId->id,
                'country_id' => $countryId->id,
                'name' => $faker->company,
                'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
                'start' => $faker->dateTimeThisDecade($max = 'now'),
                'end' => Carbon::now()->toDateTimeString(),
                'address' => $faker->address,
                'created_at' => $faker->dateTimeThisDecade($max = 'now'),
                'updated_at' => Carbon::now()->toDateTimeString()
            ];
        }

        DB::table('events')->insert($data);
    }
}
