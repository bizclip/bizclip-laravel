<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = fopen(dirname(__FILE__) . "/data/cities", "r");
        $cnt = 0;
        $data = [];

        while (($row = fgets($cities)) !== false) {
            $val = explode(";", $row);

            if (trim($val[0]) == "") continue;

            ++$cnt;

            $data[] = [
                'country_code' => trim($val[0]),
                'state_code' => $val[1] == '-' ? null : trim($val[1]),
                'name' => trim($val[2]),
                'created_at' => Carbon::now()->toDateTimeString()
            ];

            if ($cnt == 10000) {
                DB::table('cities')->insert($data);

                $cnt = 0;
                unset($data);
            }
        }

        DB::table('cities')->insert($data);

        fclose($cities);
    }
}
