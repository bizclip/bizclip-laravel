<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $data = [];

        for ($i = 0; $i < 1000; ++$i) {
            $cityId = $faker->numberBetween(1, 2559529);

            $cityRow = DB::table('cities')->where('id', $cityId)->first();
            $stateId = DB::table('states')->where('code', $cityRow->state_code)->first();
            $countryId = DB::table('countries')->where('code', $cityRow->country_code)->first();

            $data[] = [
                'city_id' => $cityId,
                'state_id' => empty($stateId) ? null : $stateId->id,
                'country_id' => $countryId->id,
                'name' => $faker->company,
                'staff_number' => $faker->numberBetween(1, 10000),
                'phone' => $faker->phoneNumber,
                'email' => $faker->email,
                'webpage' => $faker->url,
                'address' => $faker->streetAddress,
                'created_at' => $faker->dateTimeThisDecade($max = 'now'),
                'updated_at' => $faker->dateTimeThisDecade($max = 'now')
            ];
        }

        DB::table('companies')->insert($data);
    }
}
