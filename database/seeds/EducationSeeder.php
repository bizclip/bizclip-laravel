<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class EducationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $data = [];
        for ($i=0; $i<100; ++$i) {
            $data[] = [
                'name' => $faker->word,
                'created_at' => $faker->dateTimeThisDecade($max = 'now'),
                'updated_at' => Carbon::now()->toDateTimeString()
            ];
        }
        DB::table('educations')->insert($data);
    }
}
