<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = fopen(dirname(__FILE__) . "/data/states", "r");
        $data = [];

        while (($row = fgets($states)) !== false) {
            $val = explode(";", $row);

            $data[] = [
                'name' => trim($val[0]),
                'code' => trim($val[1]),
                'created_at' => Carbon::now()->toDateTimeString()
            ];
        }

        DB::table('states')->insert($data);

        fclose($states);
    }
}
