<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $data = [];

        /**
         * Standard users representing all roles
         * (for ez testing)
         */
        DB::table('users')->insert([
            [
                'first_name' => 'imenko',
                'last_name' => 'prezimenko',
                'email' => 'iprezi@foi.hr',
                'password' => bcrypt("admin"),
                'user_type_id' => 1
            ],
            [
                'first_name' => 'ana',
                'last_name' => 'pofuk',
                'email' => 'apofuk@foi.hr',
                'password' => bcrypt("user"),
                'user_type_id' => 2
            ]
        ]);

        $cnt = 0;

        for ($i = 0; $i < 10000; ++$i) {
            ++$cnt;

            $data [] = [
                'company_id' => $faker->numberBetween(1, 1000),
                'position_id' => $faker->numberBetween(1, 14),
                'user_type_id' => 2,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => $i . $faker->email,
                'password' => bcrypt("123456"),
                'birthday' => $faker->dateTime,
//                'avatar' =>
                'phone' => $faker->phoneNumber,
                'mobile' => $faker->phoneNumber,
                'years_of_experience' => $faker->numberBetween(1, 50),
//                'cv' =>
                'OIB' => $faker->isbn10,
                'created_at' => $faker->dateTimeThisDecade($max = 'now'),
                'updated_at' => $faker->dateTimeThisDecade($max = 'now')
            ];

            if ($cnt == 100) {
                DB::table('users')->insert($data);

                $cnt = 0;
                unset($data);
            }
        }
    }
}
