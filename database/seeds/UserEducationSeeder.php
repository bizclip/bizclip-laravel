<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserEducationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $data = [];
        for ($i=0; $i<50; ++$i) {
            $data[] = [
                'education_id' => $faker->numberBetween(1,100),
                'user_id' => $faker->numberBetween(1,10000),
                'created_at' => $faker->dateTimeThisDecade($max = 'now'),
                'updated_at' => Carbon::now()->toDateTimeString()
            ];
        }
        DB::table('user_educations')->insert($data);
    }
}
