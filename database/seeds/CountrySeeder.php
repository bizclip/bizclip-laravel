<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = fopen(dirname(__FILE__) . "/data/countries", "r");
        $data = [];

        while (($row = fgets($countries)) !== false) {
            $val = explode(";", $row);

            $data[] = [
                'name' => trim($val[3]),
                'code' => trim($val[0]),
                'created_at' => Carbon::now()->toDateTimeString()
            ];
        }

        DB::table('countries')->insert($data);

        fclose($countries);
    }
}
