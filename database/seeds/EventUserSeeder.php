<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class EventUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $data = [];

        for ($i = 0; $i < 10000; ++$i) {
            $data[] = [
                'code' => $i . $faker->creditCardNumber,
                'event_id' => $faker->numberBetween(1, 500),
                'user_id' => $faker->numberBetween(3, 10000),
                'created_at' => $faker->dateTimeThisDecade($max = 'now'),
                'updated_at' => Carbon::now()->toDateTimeString()
            ];
        }

        DB::table('event_users')->insert($data);
    }
}
