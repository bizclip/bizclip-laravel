<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CompanyActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $data = [];
        for ($i=0; $i<50; ++$i) {
            $data[] = [
                'company_id' => $faker->numberBetween(1, 1000),
                'activity_id' => $faker->numberBetween(1, 49),
                'created_at' => $faker->dateTimeThisDecade($max = 'now'),
                'updated_at' => Carbon::now()->toDateTimeString()
            ];
        }
        DB::table('company_activities')->insert($data);
    }
}
