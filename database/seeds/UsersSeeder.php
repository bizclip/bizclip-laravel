<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
           [
               'first_name' => 'imenko',
               'last_name'  => 'prezimenko',
               'email'      => 'iprezi@foi.hr',
               'password'   =>  bcrypt('123456')
           ]
        ]);
    }
}
