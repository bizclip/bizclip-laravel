<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(CompanyPositionSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(StateSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(UserTypeSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(EventSeeder::class);
//        $this->call(ImageSeeder::class);
        $this->call(ActivitySeeder::class);
        $this->call(CompanyActivitySeeder::class);
        $this->call(EventUserSeeder::class);
        $this->call(InterestSeeder::class);
        $this->call(UserInterestSeeder::class);
        $this->call(EducationSeeder::class);
        $this->call(UserEducationSeeder::class);
        $this->call(PacketSeeder::class);
        $this->call(TransactionSeeder::class);

        Model::reguard();
    }
}
