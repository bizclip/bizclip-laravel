<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserInterestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i = 0; $i < 1000; ++$i) {
            $data = [
                $faker->numberBetween(1, 10000),
                $faker->numberBetween(1, 49),
                $faker->dateTimeThisDecade($max = 'now'),
                Carbon::now()->toDateTimeString(),
            ];

            DB::insert(
                'INSERT INTO user_interests (`user_id`, `interest_id`, `created_at`, `updated_at`) VALUES (?, ?, ?, ?)
                 ON DUPLICATE KEY UPDATE `updated_at` = "' . Carbon::now()->toDateTimeString() .'"', $data
            );
        }
    }
}
