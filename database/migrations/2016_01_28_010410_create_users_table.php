<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned()->nullable()->default(NULL);
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('position_id')->unsigned()->nullable()->default(NULL);
            $table->foreign('position_id')->references('id')->on('company_positions');
            $table->integer('user_type_id')->unsigned()->nullable()->default(2);
            $table->foreign('user_type_id')->references('id')->on('user_types');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->dateTime('birthday')->nullable()->default(NULL);
            $table->string('avatar')->nullable()->default(NULL);
            $table->string('phone')->nullable()->default(NULL);
            $table->string('mobile')->nullable()->default(NULL);
            $table->integer('years_of_experience')->nullable()->default(NULL);
            $table->string('cv')->nullable()->default(NULL);
            $table->string('OIB')->nullable()->default(NULL);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
